 /*  Copyright(C) 2007 Christian Dietrich <stettberger@dokucode.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/
#include "config.h"

#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <avr/io.h>
#include <inttypes.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "timer.h"
#include "zbus.h"
#include "zdp.h"

/* avr-libc 1.4.x !!!! */

#define INFO_HARDWARE "m8"
#define INFO_PROTOCOL_VERSION "0.1"
#define INFO_MODULE_INFO "adc"
#define INFO_MODULE_VERSION "0.1"

uint8_t kommando;
#include "twi.c"

uint16_t adc_values[ADC_PINS];
volatile struct zbus_buffer recv_buffer;
volatile struct zbus_buffer send_buffer;

void 
zdp_stack(struct zdp_message *recv, struct zdp_message *send, uint8_t datalen)
{
  uint8_t reply = 0;
  uint8_t i;
  switch(recv->port) {
  /* device state port*/
  case 0:
    if (datalen > 0 && recv->data[0] == ZDP_GET_CMD) {
      reply = strlen(INFO_HARDWARE ";" INFO_PROTOCOL_VERSION ";" 
                     INFO_MODULE_INFO ";" INFO_MODULE_VERSION);
      memcpy(send->data, INFO_HARDWARE ";" INFO_PROTOCOL_VERSION ";" 
               INFO_MODULE_INFO ";" INFO_MODULE_VERSION, reply);
    }
    break;
  case 1:
    if (datalen > 0 && recv->data[0] == ZDP_GET_CMD) {
      /* analog port */
      for (i = 0; i < ADC_PINS; i++) {
        send->data[i * 3] = adc_values[i] >> 8;
        send->data[i * 3 + 1] = adc_values[i];
        send->data[i * 3 + 2] = ';';
      }
      reply = ADC_PINS * 3;
    }
    break;
  }
  if (reply)
    for ( i = 5; i; i--) {
      zdp_reply(recv, send, reply);
      _delay_ms(20);
    }
}



int 
main(void)
{
  BLINK_DDR |= BLINK_PIN; //LED pin auf ausgang
  BLINK_DDR |= BLINK2_PIN; //LED pin auf ausgang und aus
  BLINK_PORT &= ~BLINK2_PIN; 
  BLINK_PORT &= ~BLINK_PIN; 
  
  init_twi();
  timer_init();

  zbus_core_init();
  zbus_set_recv_buffer(&recv_buffer);
  zbus_set_send_buffer(&send_buffer);
  recv_buffer.valid = 0;
  send_buffer.valid = 0;

  zdp_set_address('a');
  
  sei();
  TWCR |= (1<<TWINT); //TWI-Modul aktiv (unbedingt nach sei() )
  
  /* Init des ADC mit Taktteiler von 64 */
  ADCSRA = _BV(ADEN) | _BV(ADPS2) | _BV(ADPS1);
  
  /* Aktivierung des Pin 0 (ADC0) fr die Messung 
  * und Spannungsreferenz auf AREF lassen*/
  ADMUX = 0x00;

  while(1) {
    /* gather adc information */
    uint8_t i;
    for (i = 0; i < ADC_PINS; i++) {
      ADMUX = i;
      /* Start adc conversion */
      ADCSRA |= _BV(ADSC);
      /* Wait for completion of adc */
      while (ADCSRA & _BV(ADSC)) {}
      adc_values[i] = ADC;
      /* Set the i2c buffer */
      i2cbuf[i * 2 + 1] = adc_values[i] >> 8;
      i2cbuf[(i + 1) * 2] = adc_values[i];
    }
    if(zdp_new_data((struct zbus_buffer *)&recv_buffer)) {
      struct zdp_message *recv_msg = (struct zdp_message *)recv_buffer.data;
      struct zdp_message *send_msg = (struct zdp_message *)send_buffer.data;
      /* Call the zdp stack */
      zdp_stack(recv_msg, send_msg, recv_buffer.len - ZDP_HEADER_LEN);
      /* Make the recv buffer invalid */
      recv_buffer.valid = 0;
    }

    timer_process();
    errortest_twi();
  }
  return 0;
}

