#ifndef _CONFIG_H
#define _CONFIG_H

#define F_CPU 8000000L

#define RXTX_PORT PORTD
#define RXTX_DDR  DDRD
#define RXTX_PIN PD2

/* use 19200 baud at 8mhz (see datasheet for other values) */
#define ZBUS_UART_UBRR 25
#define ZBUS_BUFFER_LEN 255

/* Blinking leds */
#define BLINK_PORT PORTD
#define BLINK_DDR DDRD
#define BLINK_PIN _BV(PD6)
#define BLINK2_PIN _BV(PD7)

/* I2c */
#define I2CKOMMANDO kommando
#define DEINIT_INTERUPT timer_deinit();
#ifndef TWIADDR
#   define TWIADDR 0x25
#endif

/* Adc */
#define ADC_PINS 4

/* Byte order */
#define HTONS(n) (uint16_t)((((uint16_t) (n)) << 8) | (((uint16_t) (n)) >> 8))
// #define HTONS(n) (n)
#define NTOHS(n) HTONS(n)


#endif
