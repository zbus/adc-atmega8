 /*  Copyright(C) 2007 Christian Dietrich <stettberger@dokucode.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/
#include "config.h"

#include <stdint.h>
#include <stdlib.h>

#include "zdp.h"

uint8_t zdp_address = 0;

uint16_t crcUpdate(uint16_t crc, uint8_t data)
{
        uint16_t tmp;
        uint8_t j;

	tmp = data << 8;
        for (j = 0; j < 8; j ++)
	{
                if((crc ^ tmp) & 0x8000)
			crc = (crc << 1) ^ 0x1021;
                else
			crc = crc << 1;
                tmp = tmp << 1;
        }
	return crc;
}

uint16_t 
zdp_calc_crc(struct zdp_message *message, uint8_t datalen)
{
  uint16_t crc = 0, len;
  
  crc = crcUpdate(crc, message->dst);
  crc = crcUpdate(crc, message->src);
  crc = crcUpdate(crc, message->port);
  crc = crcUpdate(crc, message->flags);
  for ( len = 0; len < datalen; len++ ) {
    crc = crcUpdate(crc, message->data[len]);
  }
  crc = crcUpdate(crc, datalen);
  return crc;
}

void 
zdp_set_address(uint8_t address)
{
  /* don't lets set an broadcast address */
  if (address == 255) return;

  zdp_address = address;
}

uint8_t 
zdp_get_address(void)
{
  return zdp_address;
}

uint8_t 
zdp_new_data(struct zbus_buffer *message) 
{
  struct zdp_message *msg = (struct zdp_message *)message->data;

  /* is message valid */
  if (!message || !message->valid)  return 0;

  /* is it for us and has an valid size */
  if (message->len < ZDP_HEADER_LEN || (msg->dst != zdp_address && msg->dst != 255)) {
    /* Make the message invalid */
    message->valid = 0;
    return 0;
  }

  /* is the crc correct */
  if (NTOHS(msg->crc) != zdp_calc_crc(msg, message->len - ZDP_HEADER_LEN)) {
    /* Make the message invalid */
    message->valid = 0;
    return 0;
  }
  return 1;
}

