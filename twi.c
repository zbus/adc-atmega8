 /*  Copyright(C) 2007 Jochen Roessner <jochen@lugrot.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#define F_CPU 8000000L
#include <avr/interrupt.h>
#include <util/delay.h>
#include <avr/io.h>
#include <avr/eeprom.h>
#include <inttypes.h>

/*i2c vars*/
volatile uint8_t byteanzahl; //zaehler der bytes beim twi empfang
volatile uint8_t runbootcode = 0xF1; //startsequenz zum bootloader start
volatile uint8_t smbuscommand; //smbus command byte
volatile uint8_t smbuscount; //bytezaehler des smbus 
volatile uint8_t Checksum; //checksumme beim eeprom schreiben
volatile uint16_t eepaddr; //eeprom schreib-start-adresse
volatile uint8_t i2cbuf[33]; //dateni2cbuffer des smbus
volatile uint8_t i2cbufaddr; //zeiger (zaehler) auf i2cbufferbyte

void
runbootloader(void)
{
#ifdef I2CBOOT512W
  void (*bootptr)( void ) = (void *) 0x0E00; // 512w bootloader
#else
  void (*bootptr)( void ) = (void *) 0x0C00; // 1024w bootloader
#endif
  DEINIT_INTERUPT
  bootptr();
}

void
init_twi(void){
  
  TWCR = 0; //fuer das Initialisieren bei einem status fehler

  /* INIT fuer den TWI i2c
  * hier wird die Addresse des µC festgelegt
  * (in den oberen 7 Bit, das LSB(niederwertigstes Bit)
  * steht dafür ob der µC auf einen general callreagiert
  */ 
#ifndef TWIADDR
#error No Twi Address specified
#endif

  TWAR = TWIADDR<<1;
  
  /* TWI Control Register, hier wird der TWI aktiviert, 
   * der Interrupt aktiviert und solche Sachen
   */
  TWCR = (1<<TWIE) | (1<<TWEN) | (1<<TWEA); 
  
  /* TWI Status Register init */
  TWSR &= 0xFC; 
}

void
errortest_twi(void){
  /* test ob ein fehler am bus vorlag und reset des twi moduls ausloesen */
  if(TWSR == 0x00){
    init_twi();
  }
}

/* Interruptroutine des TWI
 */
SIGNAL (SIG_2WIRE_SERIAL)
{
  
  switch (TWSR & 0xF8){
    case 0x80:
    /* Datenbyte wurde empfangen
     * TWDR auslesen
     */
    if (byteanzahl == 0){
      smbuscommand = TWDR;
    }
    else if (byteanzahl == 1){
      smbuscount = TWDR;
    }
    
    if(smbuscommand == 0xF0 && smbuscount == 1 && runbootcode == TWDR){
      runbootloader();
    }
    else if (smbuscommand == 0x81){
      switch (byteanzahl){
      case 0:
        i2cbufaddr = 0;
        Checksum = 0;
        break;
      case 2:
        eepaddr = TWDR;
        break;
      case 3:
        eepaddr |= TWDR<<8;
        break;
      }
    }
    else if (smbuscommand == 0x82 && byteanzahl > 1 && smbuscount > 0){
      if (i2cbufaddr < 16){
        i2cbuf[i2cbufaddr++] = TWDR;
        Checksum = (Checksum + TWDR) & 0xFF;
      }
      smbuscount--;
    }
    else if (smbuscommand == 0x40){
      if (byteanzahl > 1){
        i2cbuf[byteanzahl-2] = TWDR;
        if (--smbuscount == 0)
          I2CKOMMANDO = i2cbuf[0];
      }
    }
    byteanzahl++;
  break;
  case 0x60:
    /* Der Avr wurde mit seiner Adresse angesprochen  */
    byteanzahl = 0;
#ifdef BLINK_PORT
    BLINK_PORT |= BLINK_PIN;
#endif
  break;
  
  /* hier wird an den Master gesendet */
  case 0xA8:
    switch (smbuscommand){
      case 0x83:
        TWDR = 1;
      break;
      case 0x84:
        TWDR = 0x10;
        smbuscount = 0x00;
      break;
      case 0x44:
        TWDR = 0x20;
        smbuscount = 0x00;
      break;
      default:
        TWDR = 0x10; //zur demo den zaehler txbyte senden
      break;
    }
  break;
  case 0xB8:
    switch (smbuscommand){
      case 0x83:
        TWDR = Checksum;
      break;
      case 0x84:
        if(smbuscount < 0x10){
          TWDR = eeprom_read_byte((uint8_t *)eepaddr++);
          smbuscount++;
        }
      break;
      case 0x44:
        if(smbuscount < 0x20){
          TWDR = i2cbuf[smbuscount++];
        }
      break;
      default:
        TWDR++;
      break;
    }
    byteanzahl++;
  break;
  default:
#ifdef BLINK_PORT
    BLINK_PORT &= ~BLINK_PIN;
#endif
  break;
  /* fuer einen zukuenftigen general call */
  /* 
  if((TWSR & 0xF8) == 0x70){
      //general call
            motorschalter = 2;
  }
  */
  }
  TWCR |= (1<<TWINT); //TWI wieder aktivieren
  if(smbuscommand == 0x82 && i2cbufaddr == 16 && smbuscount == 0){
    eeprom_write_block(&i2cbuf, (void *) eepaddr, 16);
    i2cbufaddr = 0;
    smbuscommand = 0;
    eepaddr+=16;
  }
}
