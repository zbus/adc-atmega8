 /*  Copyright(C) 2007 Christian Dietrich <stettberger@dokucode.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include "config.h"

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <util/delay.h>
#include <stdlib.h>

#include "zbus.h"


static volatile uint8_t send_escape_data = 0;
static volatile uint8_t recv_escape_data = 0;
static volatile uint8_t bus_blocked = 0;

static volatile zbus_send_byte_callback_t callback = NULL;
static volatile void *callback_ctx = NULL;

static volatile struct zbus_buffer *send_buffer = NULL;
static volatile struct zbus_buffer *recv_buffer = NULL; 


void
zbus_core_init()
{
    /* set baud rate */
    UBRRH = ZBUS_UART_UBRR >> 8;
    UBRRL = ZBUS_UART_UBRR & 0xFF;

    /* set mode */
    UCSRC = _BV(URSEL) | _BV(UCSZ0) | _BV(UCSZ1);

    /* enable transmitter and receiver */
    UCSRB = _BV(RXCIE) | _BV(TXEN) | _BV(RXEN);

    /* Enable RX/TX Swtich as Output */
    RXTX_DDR |= _BV(RXTX_PIN);
    /* Default is reciever enabled*/
    RXTX_PORT &= ~_BV(RXTX_PIN);
    
}

uint8_t
zbus_send_buffer_cb(void **ctx) 
{
  (void) ctx;

  if (!send_buffer || ! send_buffer->valid ) 
    zbus_tx_finish();
  else {
    char data = send_buffer->data[send_buffer->offset];
    send_buffer->offset++;
    if (send_buffer->offset >= send_buffer->len)
      send_buffer->valid = 0;
    return data;
  }
  return 0;
}

uint8_t
zbus_send(uint8_t size) 
{
  if (bus_blocked) return 0;

  if (!send_buffer || !send_buffer->valid) {
    send_buffer->valid = 1;
    send_buffer->len = size;
    send_buffer->offset = 0;
    zbus_tx_start(zbus_send_buffer_cb, NULL);
    return 1;
  }
  return 0;
}


void
zbus_core_periodic(void)
{
  if(bus_blocked)
    bus_blocked--;
}

void
zbus_set_recv_buffer(volatile struct zbus_buffer *buffer)
{
  recv_buffer = buffer;
}

void
zbus_set_send_buffer(volatile struct zbus_buffer *buffer)
{
  send_buffer = buffer;
}


uint8_t
zbus_tx_start(zbus_send_byte_callback_t cb, void *ctx) 
{
  /* Return if there is an sending process */
  if (callback) return 0;

  /* Enable sending led */
#ifdef BLINK_PORT
  BLINK_PORT |= BLINK2_PIN;
#endif
  /* Enable transmitter */
  RXTX_PORT |= _BV(RXTX_PIN);

  /* Install send byte callback */
  callback = cb;
  callback_ctx = ctx;
  
  /* Transmit Start sequence */
  send_escape_data = ZBUS_START;
  UDR = '\\';

  /* Enable buffer empty interrupt */
  UCSRB |= _BV(UDRIE); 

  return 1;
}

void  
zbus_tx_finish(void) 
{
  callback = NULL;
  /* disable sending led */
#ifdef BLINK_PORT
      BLINK_PORT &= ~BLINK2_PIN;
#endif
}

SIGNAL(USART_UDRE_vect)
{
  _delay_ms(1);

  if (!send_buffer) return;

  if (send_escape_data) {
    UDR = send_escape_data;
    send_escape_data = 0;
    if (callback == NULL) {
      /* Wait for completion */
      while (!(UCSRA & _BV(UDRE)));
      /* Disable this interrupt */
      UCSRB &= ~_BV(UDRIE); 
      _delay_ms(1);
      /* Disable transmitter */
      RXTX_PORT &= ~_BV(RXTX_PIN);
    }
  } else {
    if (callback) {
      send_escape_data = callback((void *)callback_ctx);
      if (callback) {
        if (send_escape_data == '\\') 
          UDR = '\\';
        else {
          UDR = send_escape_data;
          send_escape_data = 0;
        }
      } else {
        /* Send Packet end */
        UDR = '\\';
        send_escape_data = ZBUS_STOP;
      }
    }
  }
}

SIGNAL(USART_RXC_vect)
{
  /* Ignore errors */
  if ((UCSRA & _BV(DOR)) || (UCSRA & _BV(FE))) {
    uint8_t v = UDR;
    (void) v;
    return; 
  }
  uint8_t data = UDR;

  /* if data is not read by application or no recieve buffer set, 
   * ignore message */
  if (!recv_buffer || recv_buffer->valid) return;

  if (data == '\\') 
    recv_escape_data = 1;
  else {
    if (recv_escape_data){
      if (data == ZBUS_START) {
        recv_buffer->len = 0;
        bus_blocked = 3;
        /* set recvieving led */
#ifdef BLINK_PORT
        BLINK_PORT |= BLINK_PIN;
#endif
      }
      else if (data == ZBUS_STOP) {
        /* clear recvieving led */
#ifdef BLINK_PORT
        BLINK_PORT &= ~BLINK_PIN;
#endif
        recv_buffer->valid = 1;
        bus_blocked = 0;
      }
      else if (data == '\\') {
        recv_escape_data = 0;
        goto append_data;
      }
      recv_escape_data = 0;
    } else {
append_data:
      /* Not enough space in buffer */
      if (recv_buffer->len >= ZBUS_BUFFER_LEN) return;
      /* If bus is not blocked we aren't on an message */
      if (!bus_blocked) return;

      recv_buffer->data[recv_buffer->len++] = data;
    }
  }
}
