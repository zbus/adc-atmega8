/*  Copyright(C) 2007 Christian Dietrich <stettberger@dokucode.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#ifndef _ZBUS_H
#define _ZBUS_H

enum zbus_escapes {
  ZBUS_START = '0',
  ZBUS_STOP = '1',
};

struct zbus_buffer {
  uint8_t valid;
  uint8_t len;
  uint8_t offset;
  uint8_t data[ZBUS_BUFFER_LEN];
};

void zbus_core_init(void);
void zbus_core_periodic(void);

typedef uint8_t (*zbus_send_byte_callback_t)(void **ctx);

void  zbus_tx_finish(void);
uint8_t zbus_tx_start(zbus_send_byte_callback_t cb, void *ctx);
void zbus_set_recv_buffer(volatile struct zbus_buffer *buffer);
void zbus_set_send_buffer(volatile struct zbus_buffer *buffer);
uint8_t zbus_send(uint8_t size);

#endif /* _ZBUS_H */
