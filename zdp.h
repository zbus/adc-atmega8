 /*  Copyright(C) 2007 Christian Dietrich <stettberger@dokucode.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#ifndef _ZDP_H
#define _ZDP_H

#include "zbus.h"

#define ZDP_HEADER_LEN 6

#define zdp_reply(recv, send, len) \
  do { if (! ((recv)->flags & ZDP_ACK)) { \
    (send)->dst = (recv)->src; \
    (send)->src = zdp_get_address(); \
    (send)->port = (recv)->port; \
    (send)->flags |= ZDP_ACK; \
    (send)->crc = HTONS(zdp_calc_crc((send), (len))); \
    zbus_send(ZDP_HEADER_LEN + (len));\
  } } while(0)

#define ZDP_GET_CMD 'g'
#define ZDP_SET_CMD 's'

/* Structure representing an zdp message */
struct zdp_message {
  uint8_t dst;
  uint8_t src;
  uint8_t port;
  uint8_t flags;

  uint16_t crc;
  char data[];
};

enum zdp_flags {
  ZDP_ACK = 1,
};

void zdp_set_address(uint8_t address);
uint8_t zdp_get_address(void);

uint8_t zdp_new_data(struct zbus_buffer *message);
uint16_t zdp_calc_crc(struct zdp_message *message, uint8_t datalen);

#endif
