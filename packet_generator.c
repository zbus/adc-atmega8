#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>

typedef unsigned char uint8_t;
typedef unsigned short int uint16_t;

struct zdp_message {
  uint8_t dst;
  uint8_t src;
  uint8_t port;
  uint8_t flags;

  uint16_t crc;
  char data[255];
};


uint16_t crcUpdate(uint16_t crc, uint8_t data)
{
        uint16_t tmp;
        uint8_t j;

	tmp = data << 8;
        for (j = 0; j < 8; j ++)
	{
                if((crc ^ tmp) & 0x8000)
			crc = (crc << 1) ^ 0x1021;
                else
			crc = crc << 1;
                tmp = tmp << 1;
        }
	return crc;
}

uint16_t 
zdp_calc_crc(struct zdp_message *message, uint8_t datalen)
{
  uint16_t crc = 0, len;
  
  crc = crcUpdate(crc, message->dst);
  crc = crcUpdate(crc, message->src);
  crc = crcUpdate(crc, message->port);
  crc = crcUpdate(crc, message->flags);
  for ( len = 0; len < datalen; len++ ) {
    crc = crcUpdate(crc, message->data[len]);
  }
  crc = crcUpdate(crc, datalen);
  return crc;
}



int 
main(int argc, char *argv[]) 
{
  int i, len = 0;
  struct zdp_message msg;
  memset(&msg, 0, sizeof(msg));
  for (i = 1; i < argc; i++) {
    if (argv[i][0] == '-') {
      if (argv[i][1] == 's') 
        msg.src = strtol(argv[i+1], NULL, 16);
      if (argv[i][1] == 'd') 
        msg.dst = strtol(argv[i+1], NULL, 16);
      if (argv[i][1] == 'p') 
        msg.port = strtol(argv[i+1], NULL, 16);
      if (argv[i][1] == 'f') 
        msg.flags = strtol(argv[i+1], NULL, 16);
      
      i++;
    } else {
      char *p = argv[i];
      while(*p) {
        msg.data[len] = *p;
        p++;
        len++;
      }
    }
  }

  msg.crc = htons(zdp_calc_crc(&msg, len));

  char *res = (char *) &msg;
  for (i = 0; i < len + 6; i++)
    putchar(res[i]);

  return 0;
}



