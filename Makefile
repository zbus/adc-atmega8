PROJECT=main
MCU=atmega8
CC=avr-gcc
I2CDEV=19640001
OBJCOPY=avr-objcopy
TWIADDR=0x25
CFLAGS=-g -mmcu=$(MCU) -Wall -Wstrict-prototypes -Os -mcall-prologues -W -DTWIADDR=$(TWIADDR)
SRC=main.c zbus.c zdp.c timer.c
OBJECTS += $(patsubst %.c,%.o,${SRC})

all:	$(PROJECT).hex

$(PROJECT).hex: $(PROJECT) packet_generator
	$(OBJCOPY) -R .eeprom -O ihex $< $@

packet_generator: packet_generator.c
	gcc packet_generator.c -o packet_generator

$(PROJECT): $(OBJECTS)
	$(CC) $(CFLAGS) -o $@ -Wl,-Map,$(PROJECT).map $(OBJECTS)
	avr-size $(PROJECT)

clean:
	rm -f *.map *.hex $(PROJECT) *.o *.s *~ packet_generator 

load: $(PROJECT).hex
	i2cloader -d $(I2CDEV) -f $(PROJECT).hex -s F0F1 -e FF -ai2c $(TWIADDR)

loadfirst: $(PROJECT).hex
	i2cloader -d $(I2CDEV) -f $(PROJECT).hex -e FF -smbusdelay

loaddude: $(PROJECT).hex
	avrdude -p $(MCU) -U flash:w:$(PROJECT).hex -E vcc,noreset

fuse:
	avrdude -p m8 -U lfuse:w:0x24:m -E vcc,noreset
