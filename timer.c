 /*  Copyright(C) 2007 Christian Dietrich <stettberger@dokucode.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/
#include "config.h"

#include <avr/interrupt.h>
#include <avr/io.h>
#include <inttypes.h>
#include <stdlib.h>
#include "zbus.h"

static uint16_t timer = 0;
static uint8_t ticked = 0;

void
timer_init(void)
{
  /* 256 Prescaler */
  TCCR2 |= _BV(CS20) | _BV(CS21) | _BV(CS22);
  /* Int. bei Overflow und CompareMatch einschalten */
  TIMSK |= _BV(TOIE2); 
}

void
timer_deinit(void)
{
  TIMSK &= ~_BV(TOIE2);
}

SIGNAL(SIG_OVERFLOW2) 
{
  timer ++;
  ticked = 1;
}

void
timer_process(void)
{
  if (ticked) {
    /* tick the zbus subsystem */
    zbus_core_periodic();
    ticked = 0;
  }
}

